//
//  CHTTheme.swift
//  A6TheFirstSwiftApp
//
//  Created by cht on 16/7/4.
//  Copyright © 2016年 cht. All rights reserved.
//

import UIKit

//MARK: 全局常用属性
public let NavBarH: CGFloat = 64
public let StatusBarH: CGFloat = 20
public let ScreenWidth: CGFloat = UIScreen.mainScreen().bounds.size.width
public let ScreenHeight: CGFloat = UIScreen.mainScreen().bounds.size.height

//MARK: cache路径
public let CHTCachePath:String = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.LibraryDirectory, NSSearchPathDomainMask.AllDomainsMask, true).last!