//
//  CHTLoggerService.swift
//  A6TheFirstSwiftApp
//
//  Created by cht on 16/6/16.
//  Copyright © 2016年 cht. All rights reserved.
//

import UIKit
import XCGLogger

let log: XCGLogger = {
    
    let log = XCGLogger.defaultInstance()
    log.xcodeColorsEnabled = true
    log.xcodeColors = [
        
        .Verbose: .lightGrey,
        .Debug:   .darkGrey,
        .Info:    .darkGreen,
        .Warning: .orange,
        .Error:   XCGLogger.XcodeColor(fg: UIColor.redColor(),bg: UIColor.whiteColor()),
        .Severe: XCGLogger.XcodeColor(fg: (255,255,255), bg: (255,0,0))
    ]
    
    #if USE_NSLOG // Set via Build Settings, under Other Swift Flags
        log.removeLogDestination(XCGLogger.Constants.baseConsoleLogDestinationIdentifier)
        log.addLogDestination(XCGNSLogDestination(owner: log, identifier: XCGLogger.Constants.nslogDestinationIdentifier))
        log.logAppDetails()
    #else
        let logPath: NSURL = cacheDirectory.URLByAppendingPathComponent("XCGLogger_Log.txt")
        log.setup(.Debug, showThreadName: true, showLogLevel: true, showFileNames: true, showLineNumbers: true, writeToFile: logPath)
    #endif

    
    return log
}()

let logHeader = "| QFCHT DEBUG |"


private let cacheDirectory: NSURL = {
    let urls = NSFileManager.defaultManager().URLsForDirectory(.CachesDirectory, inDomains: .UserDomainMask)
    return urls[urls.endIndex - 1]
}()