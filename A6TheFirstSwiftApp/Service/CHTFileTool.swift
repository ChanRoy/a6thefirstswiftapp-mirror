//
//  CHTFileTool.swift
//  A6TheFirstSwiftApp
//
//  Created by cht on 16/7/4.
//  Copyright © 2016年 cht. All rights reserved.
//

import UIKit

class CHTFileTool: NSObject {

    static let fileManager = NSFileManager.defaultManager()
    
    //计算单个文件大小
    class func fileSize(path: String) -> Double{
        
        if fileManager.fileExistsAtPath(path) {
            
            var dict = try? fileManager.attributesOfItemAtPath(path)
            if let fileSize = dict![NSFileSize] as? Int{
                
                return Double(fileSize) / 1024.0 / 1024.0
            }
        }
        return 0.0
    }
    
    //计算整个文件夹大小
    class func folderSize(path: String) -> Double{
        
        var fileSize: Double = 0
        if fileManager.fileExistsAtPath(path) {
            let childFile = fileManager.subpathsAtPath(path)
            for fileName in childFile!{
                
                let tempPath = path
                let fileFullPathName = tempPath.stringByAppendingString(fileName)
                fileSize += CHTFileTool.fileSize(fileFullPathName)
            }
            return fileSize
        }
        return 0.0
    }
    
    //清除文件，同步
    class func cleanFolder(path: String,complete:() ->()){
        
        let childFiles = fileManager.subpathsAtPath(path)
        for fileName in childFiles! {
            
            let tempPath = path
            let fileFullPathName = tempPath.stringByAppendingString(fileName)
            if fileManager.fileExistsAtPath(fileFullPathName) {
                do{
                    try fileManager.removeItemAtPath(fileFullPathName)
                }catch _{
                    
                }
            }
        }
        complete()
    }
    
    //清除文件 异步
    class func cleanFolderAsyn(path: String, complete:() ->()){
        
        let queue = dispatch_queue_create("cleanQueue", nil)
        dispatch_async(queue) { 
            let  childFiles = self.fileManager.subpathsAtPath(path)
            for fileName in childFiles!{
                
                let tempPath = path
                let fileFullPathName = tempPath.stringByAppendingString(fileName)
                if self.fileManager.fileExistsAtPath(fileFullPathName){
                    do{
                        try self.fileManager.removeItemAtPath(fileFullPathName)
                    }catch _{
                        
                    }
                }
            }
            complete()
        }
    }
    
}
