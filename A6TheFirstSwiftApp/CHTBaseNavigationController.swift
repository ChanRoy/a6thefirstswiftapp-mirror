//
//  CHTBaseNavigationController.swift
//  A6TheFirstSwiftApp
//
//  Created by cht on 16/7/4.
//  Copyright © 2016年 cht. All rights reserved.
//

import UIKit

class CHTBaseNavigationController: UINavigationController {
    
    var isAnimation = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactivePopGestureRecognizer!.delegate = nil
        
    }

    
}
