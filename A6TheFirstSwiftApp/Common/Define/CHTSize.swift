//
//  CHTSize.swift
//  A6TheFirstSwiftApp
//
//  Created by cht on 16/6/15.
//  Copyright © 2016年 cht. All rights reserved.
//

import UIKit

class CHTSize: NSObject {
    let screenHeight = UIScreen.mainScreen().bounds.size.height
    let screenWidth = UIScreen.mainScreen().bounds.size.width
    let statusBarHeight : CGFloat = 20
    let navigationBarHeight : CGFloat = 44
    let navigationBarBottom : CGFloat = 64
    let mainViewFrame = CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, UIScreen.mainScreen().bounds.size.height - 64 - 49)
    
    let zero : CGFloat = 0
    let single : CGFloat = 1
    let cellPadding : CGFloat = 15
    let lineHeight : CGFloat = 0.4
}
