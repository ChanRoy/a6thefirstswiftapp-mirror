//
//  CHTColor.swift
//  A6TheFirstSwiftApp
//
//  Created by cht on 16/6/15.
//  Copyright © 2016年 cht. All rights reserved.
//

import UIKit

func RGB(r: CGFloat, g: CGFloat, b: CGFloat) -> UIColor {
    return UIColor(red: r / 255.0, green: g / 255.0, blue: b / 255.0, alpha: 1)
}

func RGBA(r: CGFloat, g: CGFloat, b: CGFloat, a: CGFloat) -> UIColor {
    return UIColor(red: r / 255.0, green: g / 255.0, blue: b / 255.0, alpha: a)
}



// RGB颜色转换（16进制->10进制）
//#define UIColorFromRGB(rgbValue)\
//\
//[UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
//green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
//blue:((float)(rgbValue & 0xFF))/255.0 \
//alpha:1.0]

class CHTColor: NSObject {
    
    let White      = UIColor.whiteColor()       ///白色
    let Clear      = UIColor.clearColor()       ///透明色
    let Orange     = UIColor.orangeColor()      ///橙色
    let Red        = RGB(255, g: 121, b: 121)   ///红色
    let Disable    = RGB(204, g: 204, b: 204)   ///不可交互
    let Main       = UIColor.whiteColor()   ///主色
    let Title      = RGB(0,   g: 0,   b: 0)     ///标题颜色
    let Gray       = RGB(102, g: 102, b: 102)   ///深灰色
    let LightGray  = RGB(211, g: 211, b: 211)   ///浅灰色
    let Background = RGB(231, g: 232, b: 234)   ///背景颜色
    let Line       = RGB(231, g: 231, b: 231)   ///线条颜色
    let Selected   = RGB(83,  g: 125, b: 223)   ///深蓝按钮按下颜色
    let WXNavi     = RGB(48,  g: 47 , b: 53)    ///微信导航栏颜色
    let Green      = RGB(126, g: 201, b: 93)    ///绿色
    
    func getRandomColor() -> UIColor {
        return RGB(CGFloat(random() % 256), g: CGFloat(random() % 256), b: CGFloat(random() % 256))
    }
    
    class func ColorWithHex(hexColor: Int) -> UIColor {
        return UIColor(red: CGFloat((hexColor & 0xFF0000) >> 16) / 255.0, green: CGFloat((hexColor & 0xFF00) >> 8) / 255.0, blue: CGFloat(hexColor & 0xFF) / 255.0, alpha: 1)
    }
}
