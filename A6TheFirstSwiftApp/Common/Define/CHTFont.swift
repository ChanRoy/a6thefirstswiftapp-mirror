//
//  CHTFont.swift
//  A6TheFirstSwiftApp
//
//  Created by cht on 16/6/15.
//  Copyright © 2016年 cht. All rights reserved.
//

import UIKit

//public

func FONT(fontSize: CGFloat) -> UIFont {
    return UIFont.systemFontOfSize(fontSize)
}

func BOLDFONT(fontSize: CGFloat) -> UIFont {
    return UIFont.boldSystemFontOfSize(fontSize)
}

class CHTFont: NSObject {
    let F10 = FONT(10)
    let F11 = FONT(11)
    let F12 = FONT(12)
    let F13 = FONT(13)
    let F14 = FONT(14)
    let F15 = FONT(15)
    let F16 = FONT(16)
    let F17 = FONT(17)
    let F18 = FONT(18)
    let F20 = FONT(20)
    let F22 = FONT(22)
    let F24 = FONT(24)
}

//private

private let fontSpace : CGFloat = 1
private let screenWidth : CGFloat = UIScreen.mainScreen().bounds.size.width

private enum WDDeviceType{
    case iphone5
    case iphone6
    case iphone6p
}

private func thisDeviceType() -> WDDeviceType{
    if      screenWidth == 320 {return .iphone5}
    else if screenWidth == 375 {return .iphone6}
    else    {return .iphone6p}
}

//对UIFont进行扩展
private extension UIFont{
    func more(fontSize: CGFloat) -> UIFont {
        return FONT(self.pointSize + fontSize)
    }
    func less(fontSize: CGFloat) -> UIFont {
        return FONT(self.pointSize + fontSize)
    }
}

private func font(size: CGFloat) -> UIFont{
    switch thisDeviceType() {
    case .iphone5:
        return FONT(size).less(fontSpace)
    case .iphone6:
        return FONT(size)
    case .iphone6p:
        return FONT(size).more(fontSpace)
}
}

