//
//  CHT.swift
//  A6TheFirstSwiftApp
//
//  Created by cht on 16/6/15.
//  Copyright © 2016年 cht. All rights reserved.
//

import UIKit

/// 全局配置文件

let C = CHT.shareCHT

class CHT: NSObject {

    static let shareCHT = CHT()
    let Color = CHTColor()
    let Font  = CHTFont()
    let Size  = CHTSize()
    let Text  = CHTText()
    
}
