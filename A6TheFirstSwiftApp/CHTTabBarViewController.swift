//
//  CHTTabBarViewController.swift
//  A6TheFirstSwiftApp
//
//  Created by cht on 16/6/15.
//  Copyright © 2016年 cht. All rights reserved.
//

import UIKit
import BlocksKit

class CHTTabBarViewController: UITabBarController {

    static let shareInstance = CHTTabBarViewController()
    
    //首页
    let mainVC = CHTMainViewController()
    //地图
    let mapVC = CHTMapViewController()
    //代理人
    let brokerVC = CHTBrokerViewController()
    //我的
    let mineVC = CHTMineViewController()
    
    private var childConfig : NSArray{
        get {
            return [CHTTabBarItemModel(vc: mainVC, title: tabBarItemTitle.main, nImage: R.image.map_normal(), sImage: R.image.main_select()),
                    CHTTabBarItemModel(vc: mapVC, title: tabBarItemTitle.map, nImage: R.image.map_normal(), sImage: R.image.map_select()),
                    CHTTabBarItemModel(vc: brokerVC, title: tabBarItemTitle.broker, nImage: R.image.broker_normal(), sImage: R.image.broker_select()),
                    CHTTabBarItemModel(vc: mineVC, title: tabBarItemTitle.mine, nImage: R.image.mine_normal(), sImage: R.image.mine_select())]
        }
    }
    
    private struct tabBarItemTitle{
        static let main   = "首頁"
        static let map    = "地圖"
        static let broker = "代理人"
        static let mine   = "我的"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tabBar.tintColor = C.Color.White
        tabBar.backgroundColor = C.Color.White
        
        childConfig.bk_each { (x: AnyObject!) in
            let model = x as! CHTTabBarItemModel
            let nav   = CHTBaseNavigationController(rootViewController: model.vc)
            nav.tabBarItem.title = model.title
            nav.tabBarItem.setTitleTextAttributes([NSForegroundColorAttributeName: CHTColor.ColorWithHex(0xff9933)], forState: .Selected)

            nav.tabBarItem.image = model.nImage
            nav.tabBarItem.selectedImage = model.sImage?.imageWithRenderingMode(.AlwaysOriginal)
            self.addChildViewController(nav)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//class CHTTabBarItemModel: NSObject {
//    
//    var vc = UIViewController()
//    
//    var title  : String?
//    
//    var nImage : UIImage?
//    
//    var sImage : UIImage?
//    
//    class func creat(vc: UIViewController, title: String, nImage: UIImage?, sImage: UIImage?) -> CHTTabBarItemModel{
//        let item    = CHTTabBarItemModel()
//        item.vc     = vc
//        item.title  = title
//        item.nImage = nImage
//        item.sImage = sImage
//        return item
//    }
//}

class CHTTabBarItemModel: NSObject {
    
    var vc = UIViewController()
    
    var title  : String?
    
    var nImage : UIImage?
    
    var sImage : UIImage?
    
    init(vc: UIViewController, title: String, nImage: UIImage?, sImage: UIImage?){
        self.vc = vc
        self.title = title
        self.nImage = nImage
        self.sImage = sImage
    }
}

