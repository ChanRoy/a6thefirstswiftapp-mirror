//
//  CHTBrokerStoreVC.swift
//  A6TheFirstSwiftApp
//
//  Created by cht on 16/7/14.
//  Copyright © 2016年 cht. All rights reserved.
//

import UIKit

class CHTBrokerStoreVC: CHTBaseViewController {

    var model: BrokerCellModel?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "代理人店铺"
        headerView.model = model
        view.addSubview(headerView)
    }

    lazy var headerView: CHTStoreHeaderView = {
        
        let headerView = (R.nib.cHTStoreHeaderView.firstView(owner: nil, options: nil))
        return headerView!
    }()
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
