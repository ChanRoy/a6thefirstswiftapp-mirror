//
//  CHTBrokerChildCtl.swift
//  A6TheFirstSwiftApp
//
//  Created by cht on 16/6/23.
//  Copyright © 2016年 cht. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import MJRefresh

class CHTBrokerChildCtl: CHTBaseViewController {

    var parentId : String?
    var pageSize : Int = 10
    var pageIndex: Int = 0
    
    private var listTableview: UITableView! = nil
    private var brokers = [BrokerCellModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        configTableView()
        
        setupRefresh()
        
        configHud()
    }
    
    private func setupRefresh(){
        
        brokers = []
        
        let headerView = MJRefreshNormalHeader { 
            
            self.pageIndex = 1
            self.brokers.removeAll()
            self.requestData()
        }
        listTableview.mj_header = headerView
        headerView.beginRefreshing()
        
        let footerView = MJRefreshBackNormalFooter { 
            
            self.pageIndex += 1
            self.requestData()
        }
        listTableview.mj_footer = footerView
    }
    
    private func configTableView(){
        
        listTableview = UITableView(frame: CGRectMake(0, 0, C.Size.screenWidth, C.Size.screenHeight - 64 - 50 - 50), style: .Plain)
        listTableview.tableFooterView = UIView()
        listTableview.delegate = self
        listTableview.dataSource = self
        view.addSubview(listTableview)
        
        listTableview.registerNib(UINib(nibName: CHTBrokerCell.cellIndentifier(), bundle: nil), forCellReuseIdentifier: CHTBrokerCell.cellIndentifier())
        
    }
    
    private func configHud() {
        
        CHTProgressHudManager.setBackgroundColor(UIColor.blackColor())
        CHTProgressHudManager.setFont(UIFont.systemFontOfSize(16))
    }
    
    private func requestData(){

        let urlStr = "http://hk.qfang.com/qfang-api/mobile/person/nologin/queryPersonByNoLogin"
        
        var paras = [String: AnyObject]()
        if let id = parentId{
            
            paras = ["topAreaId": id]
        }
        
        paras.updateValue(pageSize, forKey: "pageSize")
        paras.updateValue(pageIndex, forKey: "pageIndex")
        
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        Alamofire.request(.GET, urlStr, parameters: paras).responseJSON() { response in
            guard let json = response.result.value else {
                
                self.listTableview.mj_header.endRefreshing()
                self.listTableview.mj_footer.endRefreshing()
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                CHTProgressHudManager.showErrorWithStatus("网络连接错误")
                return
            }
            
            if response.result.error != nil {
                self.listTableview.mj_header.endRefreshing()
                self.listTableview.mj_footer.endRefreshing()
                UIApplication.sharedApplication().networkActivityIndicatorVisible = false
                CHTProgressHudManager.showErrorWithStatus("数据连接错误")
                return
            }
            
            self.listTableview.mj_header.endRefreshing()
            self.listTableview.mj_footer.endRefreshing()
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            
            let dict = JSON(json).dictionaryObject
            
            let array = dict!["data"]!["list"]
            
            for dic in array as! [AnyObject]{
                
                let broker: BrokerCellModel = BrokerCellModel.brokerModel(dic as! NSDictionary)
                self.brokers.append(broker)
            }
            self.listTableview.reloadData()
                
            if dict!["data"]!["recordCount"]!?.integerValue == self.brokers.count {
                    
                CHTProgressHudManager.showSuccessWithStatus("没有更多数据")
            }
            
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CHTBrokerChildCtl: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return brokers.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let myCell: CHTBrokerCell = tableView.dequeueReusableCellWithIdentifier(CHTBrokerCell.cellIndentifier(), forIndexPath: indexPath) as! CHTBrokerCell

        let model = brokers[indexPath.row] as BrokerCellModel
        
        myCell.brokerModel = model
        
        return myCell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 95.0
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        let model = brokers[indexPath.row]
        
        let vc = CHTBrokerStoreVC()
        vc.model = model
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

