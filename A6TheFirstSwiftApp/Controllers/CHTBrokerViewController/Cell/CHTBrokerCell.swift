//
//  CHTBrokerCell.swift
//  A6TheFirstSwiftApp
//
//  Created by cht on 16/6/23.
//  Copyright © 2016年 cht. All rights reserved.
//

import UIKit

class CHTBrokerCell: UITableViewCell {

    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    var brokerModel: BrokerCellModel?{
        didSet{
            nameLabel.text = brokerModel?.title
            
            if let imageStr = brokerModel?.iconImageStr {
                
                iconImage.setImageWithURL(NSURL(string: imageStr), placeholder: R.image.broker_placeholedIcon())
            }
            
        }
    }

    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .None
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static func cellIndentifier() -> String{
        
        return "CHTBrokerCell"
    }
    
}

class BrokerCellModel: NSObject {
    
    var title: String?
    var iconImageStr: String?
    
    class func brokerModel(dict:NSDictionary) -> BrokerCellModel{
        
        let model = BrokerCellModel()
        model.title = dict["personName"] as? String
        model.iconImageStr = dict["pictureUrl"] as? String
        return model
    }
}