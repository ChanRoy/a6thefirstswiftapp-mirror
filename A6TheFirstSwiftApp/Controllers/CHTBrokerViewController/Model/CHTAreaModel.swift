//
//  CHTAreaModel.swift
//  A6TheFirstSwiftApp
//
//  Created by cht on 16/7/1.
//  Copyright © 2016年 cht. All rights reserved.
//

import UIKit

class CHTAreaModel: NSObject {
    
    var id: String?
    var parentId : String?
    var findex : String?
    var areaName : String?
    
    init(dict: NSDictionary) {
        
        super.init()
        
        id       = dict["id"]       as? String
        parentId = dict["parentId"] as? String
        findex   = dict["findex"]   as? String
        areaName = dict["areaName"] as? String
    }
    
    override init() {
        super.init()
    }
    
}
