//
//  CHTStoreHeaderView.swift
//  A6TheFirstSwiftApp
//
//  Created by cht on 16/7/14.
//  Copyright © 2016年 cht. All rights reserved.
//

import UIKit

class CHTStoreHeaderView: UIView {

    @IBOutlet weak var bottomGrayLine: UIView!
    @IBOutlet weak var yellowLine: UIView!
    @IBOutlet weak var topGrayLine: UIView!
    @IBOutlet weak var itemBar: UIView!
    @IBOutlet weak var saleBtn: UIButton!
    @IBOutlet weak var rentBtn: UIButton!
    @IBOutlet weak var saleBtnWidth: NSLayoutConstraint!
    @IBOutlet weak var rentBtnWidth: NSLayoutConstraint!
    
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    var model: BrokerCellModel?{
        
        didSet{
            
            iconImage.setImageWithURL(NSURL(string: (model?.iconImageStr)!), placeholder: R.image.broker_placeholedIcon())
            nameLabel.text = model?.title
        }
    }
    
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
