//
//  CHTItemView.swift
//  A6TheFirstSwiftApp
//
//  Created by cht on 16/6/22.
//  Copyright © 2016年 cht. All rights reserved.
//

import UIKit

@objc protocol CHTItemViewDelegate {
    
    optional func clickBtn(itemView: CHTItemView, index: NSInteger)
}

class CHTItemView: UIView {

    weak var delegate: CHTItemViewDelegate!
    
    var sliderView: UIView?
    
    init(frame: CGRect, itemArray: NSArray) {
        
        super.init(frame: frame)
        
        let width = frame.size.width / CGFloat(itemArray.count)
        
        for i in 0..<itemArray.count{
            
            self.frame = CGRectMake(frame.origin.x, frame.origin.y, C.Size.screenWidth, 50)
            let btn: UIButton = UIButton(type: .Custom)
            btn.frame = CGRectMake(CGFloat(i) * width, 0, width, frame.size.height)
            btn.setTitle((itemArray[i] as! String), forState: .Normal)
            btn.setTitleColor(UIColor.blackColor(), forState: .Normal)
            btn.addTarget(self, action: #selector(btnClick), forControlEvents: .TouchUpInside)
            btn.tag = 100 + i
            self.addSubview(btn)
        }
        
        let grayLine = UIView(frame: CGRectMake(0, frame.size.height - 0.5, C.Size.screenWidth, 0.5))
        grayLine.backgroundColor = UIColor.grayColor()
        addSubview(grayLine)
        
        sliderView = UIView(frame: CGRectMake(0, frame.size.height - 2, width, 2))
        sliderView!.backgroundColor = CHTColor.ColorWithHex(0xff9933)
        addSubview(sliderView!)
    }
    
    func sliderMove(index: CGFloat){
        
        let height = self.frame.height
        let width  = self.frame.width / 4
        sliderView!.frame = CGRectMake(index / 4, height - 2, width, 2)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func btnClick(sender: UIButton){
        
        delegate.clickBtn!(self, index: sender.tag - 100)
    }
    
    override func awakeFromNib() {
        
        
    }
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
