//
//  CHTMineViewController.swift
//  A6TheFirstSwiftApp
//
//  Created by cht on 16/6/16.
//  Copyright © 2016年 cht. All rights reserved.
//

import UIKit

class CHTMineViewController: CHTBaseViewController{

    var mineTable: UITableView!

    private lazy var mines: [MineCellModel] = {
        let mines = MineCellModel.loadMineCellModels()
        return mines
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configUI()
    }
    
    private func configUI(){
        
        mineTable = UITableView(frame: C.Size.mainViewFrame, style: .Grouped)
        mineTable.rowHeight = 44
//        mineTable.sectionHeaderHeight = 10
        mineTable.dataSource = self;
        mineTable.delegate = self;
        view.addSubview(mineTable)
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
        
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CHTMineViewController: UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 2
        }else{
            return 2 
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = CHTMineCell.cellFor(tableView)
        
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                cell.mineModel = mines[0]
            }else{
                cell.mineModel = mines[1]
            }
        }else{
            if indexPath.row == 0 {
                cell.mineModel = mines[2]
            }else{
                cell.mineModel = mines[3]
            }
        }
        
        return cell
    }
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        if 0 == indexPath.section {
            //
        }
        else{
            
            if 0 == indexPath.row {
                
            }else{
                
                let settingCtl = CHTSettingCtl()
                self.navigationController?.pushViewController(settingCtl, animated: true)
            }
        }

    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.1
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.separatorInset = UIEdgeInsetsZero
        
        tableView.layoutMargins  = UIEdgeInsetsZero
        
        cell.layoutMargins = UIEdgeInsetsZero
        
    }
}



