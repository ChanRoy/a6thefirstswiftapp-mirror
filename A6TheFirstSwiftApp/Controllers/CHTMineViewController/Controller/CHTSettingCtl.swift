//
//  CHTSettingCtl.swift
//  A6TheFirstSwiftApp
//
//  Created by cht on 16/7/4.
//  Copyright © 2016年 cht. All rights reserved.
//

import UIKit

class CHTSettingCtl: CHTBaseViewController {

    var tableView : UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView = UITableView(frame: CGRectMake(0, 0, C.Size.screenWidth, C.Size.screenHeight), style: .Plain)
        tableView.tableFooterView = UIView()
        tableView.delegate = self;
        tableView.dataSource = self;
        view.addSubview(tableView)
        
        tableView.registerNib(R.nib.cHTSettingCell(), forCellReuseIdentifier: CHTSettingCell.cellIndentifier())
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension CHTSettingCtl: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell: CHTSettingCell = tableView.dequeueReusableCellWithIdentifier(CHTSettingCell.cellIndentifier()) as! CHTSettingCell
        
        
        return cell
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        tableView.separatorInset = UIEdgeInsetsZero
        
        tableView.layoutMargins  = UIEdgeInsetsZero
        
        cell.layoutMargins = UIEdgeInsetsZero
        
    }
}
