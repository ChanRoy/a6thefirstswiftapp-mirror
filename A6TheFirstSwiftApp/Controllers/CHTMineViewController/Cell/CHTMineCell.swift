//
//  CHTMineCell.swift
//  A6TheFirstSwiftApp
//
//  Created by cht on 16/6/21.
//  Copyright © 2016年 cht. All rights reserved.
//

import UIKit

class CHTMineCell: UITableViewCell {
    
    var mineModel: MineCellModel?{
        didSet{
            titleLabel.text = mineModel!.title
            iconImageView.image = UIImage(named: mineModel!.iconName!)
        }
    }
    
    static private let indentifier = "CellID"
    
    class func cellFor(tableView: UITableView) -> CHTMineCell{
        
        var cell = tableView.dequeueReusableCellWithIdentifier(indentifier) as? CHTMineCell
        
        if cell == nil {
            cell = CHTMineCell(style: .Default, reuseIdentifier: indentifier)
        }
        return cell!
    }
    
    private let bottomLine = UIView()
    private lazy var iconImageView = UIImageView()
    private lazy var titleLabel = UILabel()
    private lazy var arrowView = UIImageView()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        contentView.addSubview(iconImageView)
        titleLabel.textColor = UIColor.blackColor()
        titleLabel.font = UIFont.systemFontOfSize(16)
        titleLabel.alpha = 0.8
        contentView.addSubview(titleLabel)
        
        bottomLine.backgroundColor = UIColor.grayColor()
        bottomLine.alpha = 0.15
        contentView.addSubview(bottomLine)
        
        arrowView.image = R.image.rightArrows()
        contentView.addSubview(arrowView)

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        arrowView.frame = CGRectMake(width - 20, (height - (arrowView.image?.size.height)!)/2, (arrowView.image?.size.width)!, (arrowView.image?.size.height)!)
        let rightMargin: CGFloat = 15
        let iconHeight: CGFloat = 20
        iconImageView.frame = CGRectMake(rightMargin, (height - iconHeight) / 2, iconHeight, iconHeight)
        titleLabel.frame = CGRectMake(CGRectGetMaxX(iconImageView.frame) + rightMargin, 0, 200, height)
        
        let leftMargin: CGFloat = 20
        bottomLine.frame = CGRectMake(leftMargin, height - 0.5, width - leftMargin, 0.5)
  
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}


class MineCellModel: NSObject {
    
    var title : String?
    var iconName : String?
    
    class func loadMineCellModels() -> [MineCellModel]{
        
        var mines = [MineCellModel]()
        let path = NSBundle.mainBundle().pathForResource("MinePlist", ofType: "plist")
        let arr = NSArray(contentsOfFile: path!)
        
        for dic in arr! {
            mines.append(MineCellModel.mineModel(dic as! NSDictionary))
        }
        return mines
    }
    
    class func mineModel(dic: NSDictionary) -> MineCellModel{
        
        let model = MineCellModel()
        model.title = dic["title"] as? String
        model.iconName = dic["iconName"] as? String
        return model
    }
    
    
}
