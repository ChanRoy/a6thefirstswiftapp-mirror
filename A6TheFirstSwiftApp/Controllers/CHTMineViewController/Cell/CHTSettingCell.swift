//
//  CHTSettingCell.swift
//  A6TheFirstSwiftApp
//
//  Created by cht on 16/7/4.
//  Copyright © 2016年 cht. All rights reserved.
//

import UIKit

class CHTSettingCell: UITableViewCell {

    @IBOutlet weak var fileSizeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let testStr: String = "\(CHTFileTool.fileSize(CHTCachePath))"
        print(testStr.cleanDecimalPointZero())
        
        fileSizeLabel.text = (String(format: "%.2fM", testStr.cleanDecimalPointZero()))
        
    }

    static func cellIndentifier() -> String{
        
        return "CHTSettingCell"
    }
    
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
