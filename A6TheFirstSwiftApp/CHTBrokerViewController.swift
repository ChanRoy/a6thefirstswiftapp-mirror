//
//  CHTBrokerViewController.swift
//  A6TheFirstSwiftApp
//
//  Created by cht on 16/6/16.
//  Copyright © 2016年 cht. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class CHTBrokerViewController: CHTBaseViewController {

    private var scrollView : UIScrollView!
    private var itemView   : CHTItemView!
    private let itemNames  : [String] = ["全部", "港岛", "九龙", "新界"]
    
    private var itemNameArray : [CHTAreaModel] = []
    private var itemArray : [AnyObject] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configNavBar()
        
        getAreaNameData()

    }

    
    
    private func getAreaNameData(){
    
        Alamofire.request(.GET, "http://hk.qfang.com/qfang-api/mobile/common/query/querySonArea").responseJSON() { response in
            guard let json = response.result.value else {return}
            
            let dict = JSON(json).dictionaryObject
            
            let array = dict!["data"]!["list"]
            
            for dic in array as! NSArray{
                
                let model = CHTAreaModel(dict: dic as! NSDictionary)
                self.itemNameArray.append(model)
            }
            self.appendItemNameArray()
            
            self.configUI()
        }

        
        
    }
    
    private func appendItemNameArray(){
        
        let model = CHTAreaModel()
        model.areaName = "全部"
        itemNameArray.insert(model, atIndex: 0)
    }
    
    private func configNavBar(){
        
        title = "代理人"
        let searchBtn : UIButton = UIButton(type: .Custom)
        searchBtn.frame = CGRectMake(0, 0, 40, 40)
        searchBtn.setImage(R.image.map_search(), forState: .Normal)
        searchBtn.addTarget(self, action: #selector(searchClick), forControlEvents: .TouchUpInside)
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: searchBtn)
    }
    
    private func configUI(){
    
        itemView = CHTItemView(frame: CGRectMake(0, 0, C.Size.screenWidth, 50), itemArray: itemNames)
        itemView.delegate = self
        view.addSubview(itemView)
        
        scrollView = UIScrollView(frame: CGRectMake(0, CGRectGetMaxY(itemView.frame), C.Size.screenWidth,C.Size.screenHeight - CGRectGetMaxY(itemView.frame) - 49))
        scrollView.contentSize = CGSizeMake(C.Size.screenWidth * CGFloat(itemNames.count), CGRectGetHeight(scrollView.frame))
        scrollView.pagingEnabled = true
        scrollView.bounces = false
        scrollView.showsHorizontalScrollIndicator = true
        scrollView.delegate = self;
        view.addSubview(scrollView)
        
        addChildVC()
    }
    
    private func addChildVC(){
        
        for index in 0...3{
            
            let childVC = CHTBrokerChildCtl()
            let model = itemNameArray[index]
            childVC.parentId = model.id
            addChildViewController(childVC)
            childVC.didMoveToParentViewController(self)
        }
        let childVC = childViewControllers[0]
        childVC.view.frame = CGRectMake(0, 0, C.Size.screenWidth, CGRectGetHeight(scrollView.frame))
        scrollView.addSubview(childVC.view)
    }
    
    func searchClick(btn: UIButton){
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CHTBrokerViewController: CHTItemViewDelegate{
    
    func clickBtn(itemView: CHTItemView, index: NSInteger) {
        
        let childVC = childViewControllers[index]
        childVC.view.frame = CGRectMake(C.Size.screenWidth * CGFloat(index), 0, C.Size.screenWidth, CGRectGetHeight(scrollView.frame))
        scrollView.addSubview(childVC.view)
        
        scrollView.setContentOffset(CGPointMake(C.Size.screenWidth * CGFloat(index), 0), animated: true)
    }
}

extension CHTBrokerViewController: UIScrollViewDelegate{
    
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        
        let index = NSInteger(scrollView.contentOffset.x / C.Size.screenWidth)
        
        scrollView.setContentOffset(CGPointMake(C.Size.screenWidth * CGFloat(index), 0), animated: true)
        
        clickBtn(itemView, index: index)

    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        itemView.sliderMove(scrollView.contentOffset.x)
    }
}



