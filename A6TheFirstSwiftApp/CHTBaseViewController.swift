//
//  ViewController.swift
//  A6TheFirstSwiftApp
//
//  Created by cht on 16/6/15.
//  Copyright © 2016年 cht. All rights reserved.
//

import UIKit
import YYKit


class CHTBaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        setupNavigationBar()
        
        view.backgroundColor = C.Color.White
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
//        log.info("View will appear")
    }
    
    deinit{
        print("\(className())\(title) deinit")
    }
    
    func setupNavigationBar() {
        
        colorWithNavigationBar(C.Color.Main)
        
        titleAttribute(UIColor.blackColor())
        
        if !isRootViewController() {
            
            itemWithImage(R.image.nav_Back_Btn()!, type: .Left, callBack: { [weak self](x : AnyObject!) in
                self?.popMethod()
            })
        }
    }
    
    override func loadView() {
        
        super.loadView()
        view.backgroundColor = C.Color.Background
    }
    
    
    
    func colorWithNavigationBar(color: UIColor){
        navigationController?.navigationBar.setBackgroundImage(UIImage(color: color), forBarMetrics: .Default)
    }
    
    func titleAttribute(color: UIColor, font: UIFont = BOLDFONT(17)) {
        navigationController?.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: color,NSFontAttributeName: font]
    }
    
    func removeBottomLine(){
        navigationController?.navigationBar.shadowImage = UIImage(color: C.Color.Clear)
    }
    
    
    func itemWithImage(image: UIImage, type: NavigationBarItemType, callBack: itemCallBack?){
    
        let backBtn = UIButton(type: UIButtonType.Custom)
        
        backBtn.setImage(image, forState: .Normal)
        backBtn.titleLabel?.hidden = true
        backBtn.bk_addEventHandler({ (x : AnyObject!) in
                if callBack != nil {
                    
                    callBack!(nil)
                }
            }, forControlEvents: .TouchUpInside)

        backBtn.frame = CGRectMake(0, 0, 14, 24)
        
        let item = UIBarButtonItem(customView: backBtn)
        
        item.tintColor = C.Color.White
        
        if type == .Left {
            
            self.navigationItem.setLeftBarButtonItem(item, animated: true)
        }
        else if type == .Right {
            
            self.navigationItem.setRightBarButtonItem(item, animated: true)
        }
        else{
            
            let imageView = UIImageView(image: image)
            imageView.size = image.size
            self.navigationItem.titleView = imageView
        }
        
    }
    
    func popMethod() {
        
        if self.popType == nil {
            
            self.navigationController?.popViewControllerAnimated(true)
        }else if self.popType == NavigationControllerPopType.Root.rawValue{
            
            self.navigationController?.popToRootViewControllerAnimated(true)
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
        log.warning(CHTLog("内存警告"))
    }
}

public enum NavigationBarItemType{
    case Left
    case Center
    case Right
}

public enum NavigationControllerPopType: String{
    case Default        =  "Default"
    case Root           =  "Root"
    case ViewController = "ViewController"
}


typealias itemCallBack = (AnyObject!) -> Void


private let kPopTypeKey = "kPopTypeKey"

extension UIViewController{
    
    var popType : String? {
        
        get {return objc_getAssociatedObject(self, kPopTypeKey) as? String}
        set {objc_setAssociatedObject(self, kPopTypeKey, newValue, .OBJC_ASSOCIATION_COPY_NONATOMIC)}
    }
}

extension UIViewController{
    
    private func isRootViewController() -> Bool{
        
        return self.isKindOfClass(CHTMainViewController.classForCoder()) ||
               self.isKindOfClass(CHTMapViewController.classForCoder()) ||
               self.isKindOfClass(CHTBrokerViewController.classForCoder()) ||
               self.isKindOfClass(CHTMineViewController.classForCoder())
    }
}

extension UIViewController{
    
    func CHTLog(description: String) -> String {
        return "\(logHeader) 类:\(className()) 描述:\(description)"
    }
}

