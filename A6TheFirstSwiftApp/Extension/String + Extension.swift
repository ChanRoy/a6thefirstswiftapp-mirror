//
//  String + Extension.swift
//  A6TheFirstSwiftApp
//
//  Created by cht on 16/7/4.
//  Copyright © 2016年 cht. All rights reserved.
//

import UIKit

extension String {
    
    //清除字符串小数点末尾的0
    func cleanDecimalPointZero() -> String{
        
        let newStr = self as NSString
        var s = NSString()
        
        var offset = newStr.length - 1
        while offset > 0 {
            
            s = newStr.substringWithRange(NSMakeRange(offset, 1))
            if s.isEqualToString("0"){
                offset -= 1
            }else{
                break
            }
        }
        return newStr.substringToIndex(offset + 1)
    }
}
